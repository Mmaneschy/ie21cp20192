# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Mateus Maneschy|@Mmaneschy|
|Savio Araujo|@saviodm421|

# Documentação

A documentação do projeto pode ser acessada pelo link:

> https://mmaneschy.gitlab.io/ie21cp20192/

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)